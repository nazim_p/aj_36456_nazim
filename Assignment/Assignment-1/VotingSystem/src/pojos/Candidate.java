package pojos;

public class Candidate {
	
	private String name;
	private String politicalParty;
	private long votes;
	
	public Candidate() {
		this.name = " ";
		this.politicalParty = " ";
		this.votes = 0;
	}
	
	public Candidate(String name,String politicalParty,long votes) {
		this.name = name;
		this.politicalParty = politicalParty;
		this.votes = votes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPoliticalParty() {
		return politicalParty;
	}

	public void setPoliticalParty(String politicalParty) {
		this.politicalParty = politicalParty;
	}

	public long getVotes() {
		return votes;
	}

	public void setVotes(long votes) {
		this.votes = votes;
	}

	@Override
	public String toString() {
		return "Candidate [name=" + name + ", politicalParty=" + politicalParty + ", votes=" + votes + "]";
	}
	
	

}
