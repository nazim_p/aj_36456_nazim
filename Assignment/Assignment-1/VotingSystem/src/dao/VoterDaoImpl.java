package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Voter;
import utils.DBUtils;

public class VoterDaoImpl implements IVoterDao,Closeable {

	Connection connection = null;
	PreparedStatement stmtSelect;
	
	public VoterDaoImpl() throws Exception{
		 this.connection = DBUtils.getConnection();
		 this.stmtSelect = this.connection.prepareStatement("SELECT * FROM voters");
	}
	@Override
	public String validateUser(String email, String pwd) throws Exception {
		
		try(ResultSet rs = this.stmtSelect.executeQuery()){
		//	System.out.println("inside validate user try block");
			while(rs.next()) {
				//System.out.println("inside validate user while block");
				Voter voter  = new
						Voter(rs.getString("name"),rs.getString("email"),rs.getString("password"),rs.getBoolean("status"),rs.getString("role"));
				if(voter.getEmail().equals(email) && voter.getPassword().equals(pwd)) {
					System.out.println("inside validate user if block");
					String result = voter.toString();
					//System.out.println(result);
					return result;
				}
				
			}
		}
		return null;
	}
	@Override
	public void close() throws IOException {
		try {
			connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause);
		}
		
	}
}
