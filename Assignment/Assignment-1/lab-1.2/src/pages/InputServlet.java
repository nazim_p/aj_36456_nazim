package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InputServlet
 */
@WebServlet("/Inputtest")
public class InputServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter pw=response.getWriter())
		{
			pw.print("<h5>Name :  "+request.getParameter("f1")+"</h5>");
			String[] colors = request.getParameterValues("clr");
			pw.print("<h5>Color:  </h5>");
			for(String col : colors) {
				pw.print("<h5>"+col+"</h5>");
				
			}
			pw.print("<h5>Browser:  "+request.getParameter("browser")+"</h5>");
			pw.print("<h5>City:  "+request.getParameter("myselect")+"</h5>");
			pw.print("<h5>Info:  "+request.getParameter("info")+"</h5>");
		}
	}

}
