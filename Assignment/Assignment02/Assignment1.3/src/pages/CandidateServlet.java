package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.CandidateDao;
import pojo.Candidates;

/**
 * Servlet implementation class CandidateServlet
 */
@WebServlet("/candidate")
public class CandidateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   private CandidateDao dao;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		try {
			dao= new CandidateDao();
			List<Candidates> candi=dao.listCandidates();
			try(PrintWriter pw=response.getWriter()){
				for (Candidates candidates : candi) {
					pw.print("<h5> "+candidates.toString()+"</h5>");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
