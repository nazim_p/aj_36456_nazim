package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.CandidateDao;
import Dao.VoterDao;
import pojo.Candidates;
import pojo.Voter;



/**
 * Servlet implementation class UserLoginSevlet
 */
@WebServlet("/authenticate")
public class UserLoginSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VoterDao dao;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		try {
			dao=new VoterDao();
		} catch (Exception e) {
			throw new ServletException("err in init of "+getClass().getName(), e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
			dao.cleanUp();
		} catch (Exception e) {
			//System.out.println("in destroy of"+getClass().getName()+" err"+e);
			throw new RuntimeException("err in destroy of "+getClass().getName(), e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter pw=response.getWriter()){
			String email= request.getParameter("em");
			String password= request.getParameter("pass");
			System.out.println(password);
			Voter voters= dao.authenticateVoter(email, password);
			if(voters==null) {
				pw.print("<h5> Invalid Login, Please <a href='login.html'>retry</a></h5>");
			}
			else {
				Cookie c1=new Cookie("voter_dtls",voters.toString());
				response.addCookie(c1);
				if(voters.getRole().equalsIgnoreCase("admin"))
				  response.sendRedirect("admin");
				else
				{
				   if(voters.getStatus()==0) {
					   response.sendRedirect("candidate");
				   }
				   else {
					   response.sendRedirect("voter");
				   }
				}
				
			}
		} catch (Exception e) {
		throw new ServerException("err in do-post "+getClass().getName(),e );
		}
	}

}
