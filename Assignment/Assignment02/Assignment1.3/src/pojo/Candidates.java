package pojo;

public class Candidates {
   int canId;
   String candiName;
   String candiParty;
   int votes;
   
   
public Candidates() {
	
}
public Candidates(int canId, String candiName, String candiParty, int votes) {
	this.canId = canId;
	this.candiName = candiName;
	this.candiParty = candiParty;
	this.votes = votes;
}
public int getCanId() {
	return canId;
}

public String getCandiName() {
	return candiName;
}
public void setCandiName(String candiName) {
	this.candiName = candiName;
}
public String getCandiParty() {
	return candiParty;
}
public void setCandiParty(String candiParty) {
	this.candiParty = candiParty;
}
public int getVotes() {
	return votes;
}
public void setVotes(int votes) {
	this.votes = votes;
}
@Override
public String toString() {
	return "Candidates [canId=" + canId + ", candiName=" + candiName + ", candiParty=" + candiParty + ", votes=" + votes
			+ "]";
}


   
}
