package Dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import pojo.Voter;
import utils.DBUtils;



public class VoterDao implements IVoterDao {
	  Connection connection = null;
	    PreparedStatement stmtInsert;
	    PreparedStatement stmtUpdate;
	    PreparedStatement stmtDelete;
	    PreparedStatement stmtSelect;

	    public VoterDao()throws Exception{
	        this.connection = DBUtils.fetchConnection();
	        this.stmtInsert = this.connection.prepareStatement("INSERT INTO voters VALUES(?,?,?,?,?)");
	        this.stmtUpdate = this.connection.prepareStatement("UPDATE voters SET status=? WHERE id=?");
	        this.stmtDelete = this.connection.prepareStatement("DELETE FROM voters WHERE id=?");
	        this.stmtSelect = this.connection.prepareStatement("SELECT * FROM voters  where email=? and password=?");
	    }
	    public int insert(Voter voter)throws Exception{
	        this.stmtInsert.setString(1,voter.getName());
	        this.stmtInsert.setString(2,voter.getEmail());
	        this.stmtInsert.setString(3,voter.getPassword());
	        this.stmtInsert.setShort(4,voter.getStatus());
	        this.stmtInsert.setString(5,voter.getRole());
	        return this.stmtInsert.executeUpdate();
	    }
	    public int update(int id,int status) throws Exception{
	        this.stmtUpdate.setInt(1,status);
	        this.stmtUpdate.setInt(2,id);
	        return this.stmtUpdate.executeUpdate();
	    }
	    public int delete(int id) throws Exception{
	        this.stmtDelete.setInt(1, id);
	        return this.stmtDelete.executeUpdate();
	    }
	    public List<Voter> getVoter() throws Exception{
	        List<Voter> voters = new ArrayList<Voter>();
	        try (ResultSet resultSet = this.stmtSelect.executeQuery()){
	            while (resultSet.next()){
	                Voter voter = new Voter(
	                        resultSet.getString("name"),
	                        resultSet.getString("email"),
	                        resultSet.getString("password"),
	                        resultSet.getShort("status"),
	                        resultSet.getString("role"));
	                voters.add(voter);
	            }
	        }
	        return voters;
	    }
	
		@Override
		public Voter authenticateVoter(String email, String pwd) throws Exception {
			stmtSelect.setString(1, email);
			stmtSelect.setString(2, pwd);
			try(ResultSet rst=stmtSelect.executeQuery()){
				if(rst.next())
				{
					
					return new Voter(rst.getString(2),email,pwd,rst.getShort(5),rst.getString(6));
				}
					
			}
			return null;
		}
		public void cleanUp() throws Exception{
			if(stmtInsert!=null)
				stmtInsert.close();
			if(stmtDelete!=null)
				stmtDelete.close();
			if(stmtUpdate!=null)
				stmtUpdate.close();
			if(stmtSelect!=null)
				stmtSelect.close();
			
			if(connection!=null)
				connection.close();
			System.out.println("voter Dao Cleaned up");

		}
}
