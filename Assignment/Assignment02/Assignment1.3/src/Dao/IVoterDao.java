package Dao;

import pojo.Voter;

public interface IVoterDao {
	Voter authenticateVoter(String email,String pwd)throws Exception;
}
