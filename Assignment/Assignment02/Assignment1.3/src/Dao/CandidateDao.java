package Dao;

import static utils.DBUtils.fetchConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import pojo.Candidates;


public class CandidateDao implements ICandidate {
	 private Connection cn;
	 private PreparedStatement pst1;
	 
	 public CandidateDao() throws Exception {
			cn=fetchConnection();
			pst1=cn.prepareStatement("Select * from candidates");
			System.out.println("Candidates dao created");
		}
		 

		public List<Candidates> listCandidates() throws Exception {
			List<Candidates>candidates=new ArrayList<>();
			try(ResultSet rst=pst1.executeQuery()){
				while(rst.next()) {
					Candidates candidates2=new Candidates(rst.getInt(1),rst.getString(2),rst.getString(3),rst.getInt(4));
					candidates.add(candidates2);
				}
			}
			return candidates;

		}
		public void cleanUp() throws Exception{
			if(pst1!=null)
				pst1.close();
			if(cn!=null)
				cn.close();
			System.out.println("Candidate Dao Cleaned up");

		}


}
