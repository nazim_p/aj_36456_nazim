package utils;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbUtils {
    private static final Properties properties;
    static {
        try {
            FileInputStream inputStream = new FileInputStream("config.properties");
            properties = new Properties();
            properties.load(inputStream);
            Class.forName(properties.getProperty("DRIVER"));
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(properties.getProperty("URL"),
                properties.getProperty("USER"), properties.getProperty("PASSWORD"));
    }
}
