package Dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import pojo.Voter;
import utils.DbUtils;


public class VoterDao implements Closeable {
	  Connection connection = null;
	    PreparedStatement stmtInsert;
	    PreparedStatement stmtUpdate;
	    PreparedStatement stmtDelete;
	    PreparedStatement stmtSelect;

	    public VoterDao()throws Exception{
	        this.connection = DbUtils.getConnection();
	        this.stmtInsert = this.connection.prepareStatement("INSERT INTO voters VALUES(?,?,?,?,?)");
	        this.stmtUpdate = this.connection.prepareStatement("UPDATE voters SET status=? WHERE id=?");
	        this.stmtDelete = this.connection.prepareStatement("DELETE FROM voters WHERE id=?");
	        this.stmtSelect = this.connection.prepareStatement("SELECT * FROM voters");
	    }
	    public int insert(Voter voter)throws Exception{
	        this.stmtInsert.setString(1,voter.getName());
	        this.stmtInsert.setString(2,voter.getEmail());
	        this.stmtInsert.setString(3,voter.getPassword());
	        this.stmtInsert.setShort(4,voter.getStatus());
	        this.stmtInsert.setString(5,voter.getRole());
	        return this.stmtInsert.executeUpdate();
	    }
	    public int update(int id,int status) throws Exception{
	        this.stmtUpdate.setInt(1,status);
	        this.stmtUpdate.setInt(2,id);
	        return this.stmtUpdate.executeUpdate();
	    }
	    public int delete(int id) throws Exception{
	        this.stmtDelete.setInt(1, id);
	        return this.stmtDelete.executeUpdate();
	    }
	    public List<Voter> getSingers() throws Exception{
	        List<Voter> voters = new ArrayList<Voter>();
	        try (ResultSet resultSet = this.stmtSelect.executeQuery()){
	            while (resultSet.next()){
	                Voter voter = new Voter(
	                        resultSet.getString("name"),
	                        resultSet.getString("email"),
	                        resultSet.getString("password"),
	                        resultSet.getShort("status"),
	                        resultSet.getString("role"));
	                voters.add(voter);
	            }
	        }
	        return voters;
	    }
	    @Override
	    public void close() throws IOException {
	        try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
}
