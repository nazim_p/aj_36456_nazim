package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import pojos.Candidate;

/**
 * Servlet implementation class Candidate
 */
@WebServlet("/CandidateList")
public class CandidateList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Candidate> candidates = null;
		response.setContentType("text/html");
		try(PrintWriter writer = response.getWriter()){
			
			//get session
			HttpSession session = request.getSession();
			
			//retrive Candidate Dao's
			CandidateDaoImpl dao = (CandidateDaoImpl) session.getAttribute("candidateDao");
			
			//call method
			candidates = dao.listCandidates();
			
			//Dynamic Form Generation
			writer.print("<form action='VoterStatus'>");
			for (Candidate candidate : candidates) {
				//writer.print("<h4>"+candidate.getName()+"</h4>");
				writer.print("<input type='radio' name='candidateId' value='"+candidate.getId()+"'>"+candidate.getName()+"</br>");
			}
			writer.print("<input type='submit' value='Vote'>");
			writer.print("</form>");
		}catch (Exception cause) {
			throw new ServletException("Error in doGet of "+getClass().getName(), cause);
		}
	}

}
