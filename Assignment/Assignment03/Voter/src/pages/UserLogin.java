package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Voter;

/**
 * Servlet implementation class UserLogin
 */
@WebServlet(urlPatterns = "/authenticate",loadOnStartup = 1)
public class UserLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VoterDaoImpl voterDao;
	private CandidateDaoImpl candidateDao;

	@Override
	public void init() throws ServletException {
		try {
			this.voterDao = new VoterDaoImpl();
			this.candidateDao = new CandidateDaoImpl();
		} catch (Exception cause) {
			throw new ServletException("Error in init of " + getClass().getName(), cause);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");

		try (PrintWriter writer = response.getWriter()){
			
			String email = request.getParameter("email");
			String pass = request.getParameter("pass");
			
			Voter voter = this.voterDao.validateUser(email, pass);
			
			if(voter == null)//send Retry link
				writer.print("<h4>Invalid Login Details</h4><h5><a href='login.html'>Retry</a></h5>");
			else {
				
				//get Session
				HttpSession session = request.getSession();
				
				session.setAttribute("voterDao", voterDao);
				session.setAttribute("candidateDao", candidateDao);
				session.setAttribute("user", voter);
				
				//check user Role
				if(voter.getRole().equalsIgnoreCase("admin"))
					response.sendRedirect("admin");
				else {
					
					//check voting status
					if(voter.isStatus())
						response.sendRedirect("VoterStatus");
					else
						response.sendRedirect("CandidateList");
				}
			}
			
		} catch (Exception cause) {
			throw new ServletException("Error in doPost of " + getClass().getName(), cause);
		}

	}

	@Override
	public void destroy() {
		try {
			this.voterDao.cleanUp();
			this.candidateDao = new CandidateDaoImpl();
		} catch (Exception cause) {
			throw new RuntimeException("Error in destroy of " + getClass().getName(), cause);
		}
	}

}
