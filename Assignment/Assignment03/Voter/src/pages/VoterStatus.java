package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Voter;

/**
 * Servlet implementation class VoterStatus
 */
@WebServlet("/VoterStatus")
public class VoterStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter writer = response.getWriter()) {
			// http://localhost:8080/Voter/VoterStatus?candidateId=4

			// get session
			HttpSession session = request.getSession();

			// get voter
			Voter voter = (Voter) session.getAttribute("user");

			// check status
			if (!voter.isStatus()) {
				CandidateDaoImpl candidateDao = (CandidateDaoImpl) session.getAttribute("candidateDao");
				VoterDaoImpl voterDao = (VoterDaoImpl) session.getAttribute("voterDao");
				int candidateId = Integer.parseInt(request.getParameter("candidateId"));

				if (candidateDao.incrementVotes(candidateId)) {
					if(voterDao.updateVotingStatus(voter.getId()))
						writer.print("<h4>" + voter.getName() + " you have voted!!!</h4>");
				}
			} else {
				writer.print("<h4>" + voter.getName() + " you have already voted!!!</h4>");
			}
			session.invalidate();
		} catch (Exception cause) {
			throw new ServletException("Error in doGet of "+getClass().getName(), cause);
		}
	}

}
