package dao;

import java.sql.*;

import pojos.Voter;
import static utils.DBUtils.fetchConnection;

public class VoterDaoImpl implements IVoterDao {
	
	private Connection connection = null;
	private PreparedStatement select;
	private PreparedStatement update;
	
	public VoterDaoImpl() throws Exception{
		this.connection = fetchConnection();
		this.select = connection.prepareStatement("select id,name,email,status,role from voters where email=? and password=?");
		this.update = connection.prepareStatement("update voters set status=true where id=?");
		System.out.println("Voter Dao Created");
	}

	@Override
	public Voter validateUser(String email, String password) throws Exception {
		this.select.setString(1, email);
		this.select.setString(2, password);
		try(ResultSet set = this.select.executeQuery()){
			if(set.next())
				return new Voter(set.getInt("id"), set.getString("name"), email, password, set.getBoolean("status"), set.getString("role"));
		}
		return null;
	}
	
	@Override
	public boolean updateVotingStatus(int voterId) throws Exception {
		this.update.setInt(1, voterId);
		this.update.executeUpdate();
		int count = this.update.getUpdateCount();
		if(count > 0)
			return true;
		return false;
	}
	
	public void cleanUp() throws Exception {
		
		if(this.select != null)
			this.select.close();
		
		if(this.connection != null)
			this.connection.close();
		System.out.println("Voter dao cleaned up...");
	}

	

}
