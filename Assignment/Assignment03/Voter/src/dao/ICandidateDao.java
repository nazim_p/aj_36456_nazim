package dao;

import java.util.List;

import pojos.Candidate;

public interface ICandidateDao {
	public List<Candidate> listCandidates() throws Exception;
	public boolean incrementVotes(int candidateId) throws Exception;
}
