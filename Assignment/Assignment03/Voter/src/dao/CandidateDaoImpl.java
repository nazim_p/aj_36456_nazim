package dao;

import static utils.DBUtils.fetchConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import pojos.Candidate;

public class CandidateDaoImpl implements ICandidateDao {

	private Connection connection = null;
	private PreparedStatement select;
	private PreparedStatement update;

	public CandidateDaoImpl() throws Exception {
		this.connection = fetchConnection();
		this.select = connection.prepareStatement("select * from candidates");
		this.update = connection.prepareStatement("update candidates set votes=votes+1 where id=?");
		System.out.println("Candidate Dao Created");
	}

	@Override
	public List<Candidate> listCandidates() throws Exception {
		List<Candidate>candidates = new ArrayList<Candidate>();
		try(ResultSet set = this.select.executeQuery()){
			while(set.next())
				candidates.add(new Candidate(set.getInt("id"), set.getString("name"), set.getString("party"), set.getInt("votes")));
		}
		return candidates;
	}
	
	@Override
	public boolean incrementVotes(int candidateId) throws Exception {
		this.update.setInt(1, candidateId);
		this.update.executeUpdate();
		int count = this.update.getUpdateCount();
		if(count > 0)
			return true;
		return false;
	}

	public void cleanUp() throws Exception {

		if (this.select != null)
			this.select.close();

		if (this.connection != null)
			this.connection.close();
		System.out.println("Candidate dao clean up");
	}

	

}
