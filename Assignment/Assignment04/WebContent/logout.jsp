<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Log Out</title>
</head>
<body>
	<h4>Hello,${sessionScope.user_details.name}</h4>
	<%
		session.invalidate();
	%>
	<h4>You have successfully Logout!!!</h4>
	<h5>
		<a href="login.jsp">Visit Again</a>
	</h5>
</body>
</html>