<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h5>JSession ID : ${pageContext.session.id}</h5>
<%--cookie.get("JSESSIONID").getValue() --%>
<h5>JSessionID via cookie ${cookie.JSESSIONID.value}</h5>
	<%--Display attributes from different scopes : using EL syntax --%>
	<h5>Page Scoped Attr : ${pageScope.attr1}</h5>
	<h5>Request Scoped Attr : ${requestScope.attr2}</h5>
	<h5>Session Scoped Attr : ${sessionScope.user_details}</h5>
	<h5>Application Scoped Attr : ${applicationScope.attr3}</h5>
	<h5>Request Param : ${param.name}</h5>
</body>
</html>