package pojos;

import java.sql.Date;

public class Customer {
private int customerid;
private String email,password;
private double regAmount;
private Date regDate;
public Customer() {
	// TODO Auto-generated constructor stub
}
public Customer(int customerid, String email, String password, double regAmount, Date regDate) {
	super();
	this.customerid = customerid;
	this.email = email;
	this.password = password;
	this.regAmount = regAmount;
	this.regDate = regDate;
}
public int getCustomerid() {
	return customerid;
}
public void setCustomerid(int customerid) {
	this.customerid = customerid;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public double getRegAmount() {
	return regAmount;
}
public void setRegAmount(double regAmount) {
	this.regAmount = regAmount;
}
public Date getRegDate() {
	return regDate;
}
public void setRegDate(Date regDate) {
	this.regDate = regDate;
}
@Override
public String toString() {
	return "Customer [customerid=" + customerid + ", email=" + email + ", password=" + password + ", regAmount="
			+ regAmount + ", regDate=" + regDate + "]";
}

}
