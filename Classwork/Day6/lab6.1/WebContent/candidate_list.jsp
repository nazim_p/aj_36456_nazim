<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%--display list of candidates : by calling B.L method of candidate bean : EL syntax --%>
<%--session.getAttribute("candidate").getCandidates() --%>
<h5> Candidate List ${sessionScope.candidate.candidates} </h5>
</body>
</html>