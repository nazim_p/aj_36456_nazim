<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>	
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>

<%--ALL MATCHING setters of bean called by WC--%>
<jsp:setProperty property="*" name="voter" />
<body>
	<%--invoke B.L method of bean --%>
	<%-- below content will NOT be sent to clnt  --%>
	<%-- <h5>Status : ${sessionScope.voter.validateUser()}</h5> --%>
	<%-- <h5>Validated user details : ${sessionScope.voter.validatedUser}</h5> --%>
	<%-- redirecting the clnt in the NEXT  request to next page  --%>
	<%--response.sendRedirect + URL rewriting  --%>
	<c:redirect url="${sessionScope.voter.validateUser()}.jsp" />
</body>
</html>