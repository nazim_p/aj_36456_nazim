package pages;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.*;
import javax.servlet.http.*;
@WebServlet(value= {"/test","/test2"},loadOnStartup = 1)
public class HelloServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("in doGet "+Thread.currentThread());
		resp.setContentType("text/html");
	try(PrintWriter pw=resp.getWriter()) {
		pw.print("<h4>Hello"+LocalDateTime.now()+"</h4>");
		
	} catch (Exception e) {
		// TODO: handle exception
	}
		super.doGet(req, resp);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		System.out.println("in destroy"+Thread.currentThread());
		super.destroy();
	}

	@Override
	public void init() throws ServletException {
		System.out.println("in init"+Thread.currentThread());
		super.init();
	}
	
}
