package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDaoImpl;
import pojos.Customer;

/**
 * Servlet implementation class CategoryServlet
 */
@WebServlet("/category")
public class CategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h5>Successful Login </h5>");
			// get HttpSession from WC
			HttpSession hs=request.getSession();
			System.out.println("From category page HS "+hs.isNew());//false
			System.out.println("Session ID "+hs.getId());//SAME session id 
			//get customer details from session scope
			Customer customer=(Customer)hs.getAttribute("user_details");
			//get book dao instance from session scope
			BookDaoImpl bookDao=(BookDaoImpl) hs.getAttribute("book_dao");
			//invoke BookDao's method to fetch all categories
			List<String> categories = bookDao.getAllCategories();
			if(customer != null) {
				//implies that session tracking is working fine
				pw.print("<h5> Hello ,  "+customer.getEmail()+"</h5>");
				pw.print("<form action='category_details'>");
				pw.print("Choose Category ");
				pw.print("<select name='cat_name'>");
				for(String s : categories)
					pw.print("<option value="+s+">"+s+"</option>");
				pw.print("</select><br>");
				//add submit button : choose
				pw.print("<input type='submit' value='Choose'>");
				pw.print("</form>");
				
			}
			else
				pw.print("<h5> No Cookies , Session Tracking failed!!!!!</h5>");
			
			
			// send logout link to the clnt
			pw.print("<h5><a href='logout'>Log Me Out</a></h5>");
		} catch (Exception e) {
			//re throw the exc to WC :  by wrapping it in ServletException
			throw new ServletException("err in do-get of "+getClass().getName(), e);
		}
	}

}
