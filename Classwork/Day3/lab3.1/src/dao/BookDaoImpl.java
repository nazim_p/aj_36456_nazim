package dao;

import java.util.ArrayList;
import java.util.List;

import java.sql.*;
import pojos.Book;
import static utils.DBUtils.fetchConnection;

public class BookDaoImpl implements IBookDao {
	private Connection cn;
	private PreparedStatement pst1, pst2, pst3;

	// constr : will be invoked by a servlet
	public BookDaoImpl() throws Exception {
		// get cn from DBUtils
		cn = fetchConnection();
		// pst : for getting all categories
		pst1 = cn.prepareStatement("select distinct category from dac_books");
		// pst : to fetch all book from a selected category
		pst2 = cn.prepareStatement("select * from dac_books where category=?");
		// pst : to fetch actual book details selected by the user(from the cart)
		pst3 = cn.prepareStatement("select * from dac_books where id=?");
		System.out.println("book dao created...");
	}

	// clean up method to release DB resources
	public void cleanUp() throws Exception {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (cn != null)
			cn.close();
		System.out.println("book dao cleaned up....");
	}

	@Override
	public List<String> getAllCategories() throws Exception {
		// create empty ArrayList to hold the category names
		List<String> categories = new ArrayList<>();
		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				categories.add(rst.getString(1));
		}
		return categories;
	}

	@Override
	public List<Book> getAllBookByCategory(String categoryName) throws Exception {
		List<Book> availableBooks = new ArrayList<>();
		// set IN param : category name
		pst2.setString(1, categoryName);
		try (ResultSet rst = pst2.executeQuery()) {
			while (rst.next())
				availableBooks.add(
						new Book(rst.getInt(1), rst.getString(2), rst.getString(3), 
								categoryName, rst.getDouble(5)));
		}
		return availableBooks;
	}

	@Override
	public Book getBookDetails(int bookId) throws Exception {
		//Set IN param : book id
		pst3.setInt(1, bookId);
		try(ResultSet rst=pst3.executeQuery())
		{
			if(rst.next())
				return new Book(bookId, rst.getString(2), rst.getString(3), 
						rst.getString(4), rst.getDouble(5));
		}
		return null;
	}

}
